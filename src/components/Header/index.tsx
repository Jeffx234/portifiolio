import { useContext } from 'react';
import { GiHamburgerMenu } from 'react-icons/gi';
import Switch from 'react-switch';
import { ThemeContext } from 'styled-components';
import { Container } from "./styles";

interface Props {
  toggleTheme: () => void;
}

export function Header ({toggleTheme}: Props) {
  const { title } = useContext(ThemeContext)

  return(
    <Container>
      <nav>
        <h1> Portifólio </h1>
       
        <ul>
          <a href="inicio">
            <li>Início</li>
          </a>

          <a href="sobre">
            <li>Sobre</li>
          </a>

          <a href="contato">
            <li>Contato</li>
          </a>

          <a href="projeto">
            <li> Projetos </li>
          </a>
          <Switch 
          onChange={toggleTheme}
          checked={title === 'dark'}
          checkedIcon={false}
          height={25}
          uncheckedIcon={false}
          onColor={'#222'}
        />  
        </ul>  
        <div className="toggle">
            <GiHamburgerMenu size="30" />
        </div>
   
      </nav>
   
    </Container>
  )
}