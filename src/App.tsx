import { ThemeProvider, DefaultTheme } from 'styled-components'
import { Header } from './components/Header'
import { CoolPage } from './components/ScrollToTop'
import { Dashboard } from './Dashboard'
import { GlobalStyled } from './styles/global'
import dark from './styles/themes/dark'
import light from './styles/themes/light'
import usePersistedState from './utils/usePersistedState'

export function App() {
  const [theme, setTheme] = usePersistedState<DefaultTheme>('theme', dark)

  const toggleTheme = () => {
    setTheme(theme.title === 'light' ? dark : light)
  }

  return (

      <ThemeProvider theme={theme}>
          <CoolPage/> 
          <GlobalStyled />
          <Header toggleTheme={toggleTheme} />
          <Dashboard />
      </ThemeProvider>
  )
}