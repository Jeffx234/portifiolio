
import { BsArrowDownCircle } from 'react-icons/bs';
import { FaWhatsapp } from 'react-icons/fa';
import { Button } from '../../components/Button';
import logo from '../../images/logo.png';
import { Container, Image, Name } from './styles';


export function Title ()  {

  return (
    <Container>
      <Name>
        <p> Olá, Eu sou </p>
        <div className="rocket">
          <h1> Jeferson Luis</h1>
        </div>

        <p className="position"> Desenvolvedor Front-end <span> Health Mobile</span> </p>
        <div className="buttons">
            <Button 
              image={ <FaWhatsapp color='green' size="15"/> }
              title="Contato" 
            />
      
          <Button 
            image={ <BsArrowDownCircle color="yellow" size="15"/> }
            title="Curriculo"
          />
          
        </div>
      </Name>
       
        <Image>
           <img src={logo} alt="" />
        </Image>
    </Container>
  )
}

