import { Footer } from "./components/Footer";
import { Projetos } from "./components/Projetos";
import ScrollReveal from "./components/ScrollReveal";
import { Skills } from "./components/Skills";
import { Sobre } from "./components/Sobre";
import { Title } from "./components/Title";
import { Hr } from "./styles/global";

export function Dashboard() {
  return(
    <>
      <div className="container">
      <ScrollReveal >
        <Title />
      </ScrollReveal>  
        <Hr />

      <ScrollReveal >
        <Sobre />
       </ScrollReveal>
          <Hr/>
        <Projetos/>
        <Hr/>
        <Skills/>
      </div>
      <Footer/>
    </>
  
  )
}