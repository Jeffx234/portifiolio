import ScrollReveal from '../ScrollReveal';
import { AspectRatioDemo } from './radixImg';
import { Container, Content } from "./styles";
import ignewImg from '../../images/ignews.png'

export function Projetos() {
  return (
    <Container>
      <h1 className="proj"> Projetos </h1>
      <Content>
        <ScrollReveal>  
          <AspectRatioDemo 
            img={ignewImg} alt="Projeto ignews"
            title="Projeto ignews"
            descricao="Ignews é um uma plataforma de pagamento"
            tecnologia="Tecnologias usadas"
          />
          <AspectRatioDemo img={ignewImg} alt="Projeto ignews"/>
        </ScrollReveal>
            
          <ScrollReveal>
            <AspectRatioDemo img={ignewImg} alt="Projeto ignews"/>
            <AspectRatioDemo img={ignewImg} alt="Projeto ignews"/>
          </ScrollReveal>      
      </Content>
    </Container>
  )
}