import styled from "styled-components";

export const Container = styled.section `
padding: 0;
margin-top: 8rem;

  .proj {
    font-size: 2rem;
    text-align: center;
    margin-bottom: 2rem;
  }
`

export const Content = styled.div `
  margin: auto;
  display: grid;
  grid-template-columns: repeat(2, 1fr);


  @media (max-width: 700px) {
    grid-template-columns: 1fr;
  }

 
`

