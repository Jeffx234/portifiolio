import {
  AiFillGithub, AiFillLinkedin, AiOutlineInstagram, AiOutlineMail, AiOutlineWhatsApp
} from 'react-icons/ai'
import { Icon } from './styles'



export function IconName() {
  return(
    <Icon>
      <div className="item">
        <a href="https://github.com/Jeffx234" target="_blank" rel="noreferrer">
          <AiFillGithub size={30} color="blue"/>
          <p> Github </p>
        </a>
      </div>

      <div className="item">
        <a href="https://www.instagram.com/jefersonluis91/" target="_blank" rel="noreferrer">
          <AiOutlineInstagram size={30} color="blue"/>
          <p> Instagram </p>
        </a>
      </div>

      <div className="item">
        <a href="https://www.linkedin.com/in/jefersonluisx/" target="_blank" rel="noreferrer">
          <AiFillLinkedin size={30} color="blue"/>
          <p> Linkedin </p>
        </a>
      </div>

      <div className="item">
        <a href="https://api.whatsapp.com/send?phone=5521966591901" target="_blank" rel="noreferrer">
          <AiOutlineWhatsApp size={30} color="blue"/>
          <p> Whatszap </p>
        </a>
      </div>

      <div className="item">
        <a href="jefersonluisx@hotmail.com" >
          <AiOutlineMail size={30} color="blue"/>
          <p> Email </p>
        </a>
      </div>
    </Icon>
  )
}
