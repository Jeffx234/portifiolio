
export interface IButtonProps {
  title: string;
  descricao?: string;
  image?: Object;
}
