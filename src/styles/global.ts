import styled, { createGlobalStyle } from 'styled-components'

export const GlobalStyled = createGlobalStyle`

html {
  scroll-behavior: smooth;

  @media (max-width: 1080px) {
    font-size: 93.75%; //15px;
  }

  @media (max-width: 720px) {
    font-size: 87.5%; //14px;
  }

}

body, input, textarea, button {
  font-family: 'Poppins', sans-serif;
  font-weight: 400;
}

h1, h2, h3, h4, h5, h6, strong {
  font-weight: 600;
}


  button {
    cursor: pointer;
  }

[disabled] {
  opacity: 0.6;
  cursor: not-allowed;
}


* {
  
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

body {
  color: #fff;
  font-family: 'Opens-sans', sans-serif;
  -webkit-font-smoothing: antialiased;
  background: ${props => props.theme.colors.background};
  color: ${props => props.theme.colors.text}
}

.container {
  max-width: 1480px;
  padding: 0 2rem;
}


`

export const Hr = styled.div `
  margin-top: 5rem;
  width: 100%;
  background: ${props => props.theme.colors.secundary};
  height: 2px;

`

