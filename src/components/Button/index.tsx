import { Container } from "./styles";


interface IProps {
  title: string;
  descricao?: string;
  image?: Object;
}



export function Button( {title, descricao, image}: IProps ) {
  return (

  
    <Container type="button">
      <div className="button-img">
        <div className='img'>
          { image }
        </div>
        <div className="title">
          { title }
        </div>
      </div>
    </Container>
  )
}