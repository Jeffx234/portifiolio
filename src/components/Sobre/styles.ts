import styled from "styled-components";

export const Container = styled.section `
  display: flex;
  margin-top: 8rem;
  justify-content: space-between;

  button {
    margin-left: 1rem;
  }

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  .item {
    p {
      margin-top: 5px;
    }

    a {
      transition: filter 0.2s;
      text-decoration: none; 
      color: ${props => props.theme.colors.text}     
    }

      a:hover {
        filter: brightness(0.6);
    }
  }

  @media(max-width: 700px) {
    display: grid;


    img {
      width: 100%;
      height: 100%;
    }
 
  }
`

export const Foto = styled.div`
  width: 300px;
  border-radius: 4px;
  border-left: 4px solid ${props => props.theme.colors.secundary};
  margin-right: 1rem;
  margin-bottom: 1rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: .2rem;


  img {
    width: 100%;
  }

 

  @media(max-width: 750px) {
    width: 100%;
    height: 230px;
  }

  @media(min-width: 1048px) {
    width: 367px;
    height: 367px;
  }

  
 
`

export const SobreMim = styled.div`
  width: 50%;
  text-align: center;

  h2 {
    margin: 40px 0;
    font-size: .9rem;
  }

  @media(max-width: 700px) {
    margin-top: 2rem;
    width: 100%;
  }
 
`

export const Icon = styled.div `
  margin: 40px 0;
  display: flex;
  justify-content: space-around;

  @media(max-width: 700px) {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    gap: 2rem;
  }

`
