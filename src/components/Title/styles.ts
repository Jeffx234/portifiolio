import styled from "styled-components";

export const Container = styled.main `
  width: 100%;
  padding-top: 6rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  

  @media(max-width: 750px) {
    border: 1px solid red;

    h1 {
      text-align: center;
    }
  }

  @media(max-width: 1024px) {
    padding-top: 10rem;

  }
`;



export const Name = styled.div`
width: 50%;

p {
  font-size: 1.5rem;
}
  
  h1 {
    font-size: 2.5rem;
  }

  p{
    margin-bottom: 1rem;
    span {
      color: red;
      font-weight: bold;
    }
  }


  .buttons {
    display: flex;
    width: 100%;
    margin: auto;

    button {
      width: 46%;
      margin-right: 1rem;
      background: #141452;
    }
  }

  .contato {
    margin-left: 1rem;
    text-align: center;

    .whats {
      margin-right: .3rem;
      margin-top: .1rem;
    }
  }

  

  img {
    width: 100px;
  }

  .position {
    margin-top: 1rem;
    font-size: 1.5rem;
  }




/* Responsividade */
  @media(max-width: 750px) {
    text-align: center;
    width: 100%;


    h1 {
    font-size: 2.3rem;
    
  }

  p{
    margin-bottom: 1rem;

    span {
      color: red;
      font-weight: bold;
    }
  }

  
  button {
    padding: .8rem 2rem;
    cursor: pointer;
    transition: filter 0.2s;
    border-radius: 8px;
  }

 
}
  
`


export const Image = styled.div `
  margin-left: 2rem;

  @media(max-width: 750px) {
    img {
      display: none;
    }
  }
   
  

  img {
    width: 100%;
  }



`